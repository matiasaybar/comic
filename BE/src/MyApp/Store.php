<?php
namespace MyApp;
use Ratchet\MessageComponentInterface;
use Ratchet\ConnectionInterface;

class Store implements MessageComponentInterface {
    protected $clients, $con, $db;

    public function __construct() {
        $this->clients = new \SplObjectStorage;
        $this->con = mysqli_connect('localhost', 'root', '', 'comic');
    }

    public function onOpen(ConnectionInterface $conn) {
        // Store the new connection to send messages to later
        $this->clients->attach($conn);

        echo "New connection! ({$conn->resourceId})\n";
    }

    public function onMessage(ConnectionInterface $conn, $message) {
        // send message out to all connected clients
        $message = (array)json_decode($message);
        $info = new \stdClass();
        switch ($message['type']) {
            case 'connection':
                $sql = sprintf("SELECT idusers, user, password, token FROM users WHERE user = '%s' AND password = '%s' LIMIT 0,1 ",
                    mysqli_real_escape_string($this->con, $message['user']),
                    mysqli_real_escape_string($this->con, $message['pass']));
                $result = mysqli_query($this->con, $sql);
                if(mysqli_num_rows($result)) {

                    $dato = mysqli_fetch_array($result);

                    // Genero el token
                    $newToken = md5(time());
                    mysqli_query($this->con, "UPDATE users SET token = '".$newToken."' WHERE idusers = " . $dato['idusers']);

                    $info->user = $dato['user'];
                    $info->email = "matias.aybar@globant.com"; // Mail hardcodeado :P
                    $info->token = $newToken;
                    $info->success = true;
                    $info->message = 'Conexión exitosa';


                }
                else {
                    $info->user = "";
                    $info->email = "";
                    $info->token = null;
                    $info->success = false;
                    $info->message = 'Datos erroneos';
                }
                break;

                case 'comics':
                // Validar usuario
                $sqlToken = sprintf("SELECT token FROM users WHERE token = '%s' LIMIT 0, 1",
                    mysqli_real_escape_string($this->con, $message['token']));
                //echo $sqlToken;
                $resultToken = mysqli_query($this->con, $sqlToken);

                if(mysqli_num_rows($resultToken)) {

                    $sql = "SELECT ch.name AS name_character, c.title, c.description, c.images, c.edition, g.name AS name_genre
                        FROM comics c 
                        LEFT JOIN genres g ON c.idgenres = g.genres 
                        LEFT JOIN characters ch ON c.idcharacters = ch.idcharacters";
                    $result = mysqli_query($this->con, $sql);
                    $list = Array();
                    while( $row = mysqli_fetch_object( $result ) ) {
                        /*
                        {
                            character: 'Batman',
                            title: 'Batman - Dark kninght',
                            description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
                            edition: 'standard',
                            genre: 'superhero',
                            image: 'img/batman.jpg'
                        }
                        */
                        $comicClass = new \stdClass();
                        $comicClass->character = $row->name_character;
                        $comicClass->title = $row->title;
                        $comicClass->description = $row->description;
                        $comicClass->genre = $row->name_genre;
                        $comicClass->edition = $row->edition;
                        $comicClass->img = $row->images;
                        array_push($list, $comicClass);
                        unset($comicClass);
                    }
                    $info = $list;

                }
                else {
                    $info = new \stdClass(); $info->character = "ERROR";
                }
                break;
            
            default:
                # code...
                break;
        }

        $message = json_encode($info);

        foreach ($this->clients as $client) {
            $client->send($message);
        }
    }

    public function onClose(ConnectionInterface $conn) {
        // The connection is closed, remove it, as we can no longer send it messages
        $this->clients->detach($conn);

        echo "Connection {$conn->resourceId} has disconnected\n";
    }

    public function onError(ConnectionInterface $conn, \Exception $e) {
        echo "An error has occurred: {$e->getMessage()}\n";

        $conn->close();
    }
}