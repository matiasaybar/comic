var App;
(function( App ) {

	var Ws = (function() {

		function Ws() {
			this.url = '';
			this.connOpen = false;
			this.myWebSocket = {};
			this.tokenValue = null;
		}

		Ws.prototype.create = function() {
			if( this.isAvailable() ) {
				this.myWebSocket = new WebSocket('ws://localhost:8080');
				var that = this;
				this.myWebSocket.onopen = function( e ) {
					that.connOpen = true;
				};
				this.myWebSocket.onerror = function( e ) {
					alert(e);
					return false;
				};
			}
			else {
				alert('No está permitida la conexión vía WebSocket');
				return false;
			}
			return this.myWebSocket;
		}

		Ws.prototype.isAvailable = function() {
			return window.WebSocket;
		}

		Ws.prototype.login = function( user, pass, callbackFn ) {
			var that = this;
			var r = {};
			if( that.connOpen ) {
				that.myWebSocket.send(JSON.stringify({user:user, pass:pass, type: 'connection'}));
				that.myWebSocket.onmessage = function( e ) {
					var r = JSON.parse(e.data);
					that.tokenValue = r.token;
					callbackFn( r ); // Ejecuto el callback
				}
				that.myWebSocket.onerror = function( e ) {
					alert(':(');
				}
				// Dummy access
				/*
				if(user == 'test' && pass == 'test') {
					r = {
						success: true,
						token: '123456',
						message: 'Conexión exitosa.',
						email: 'matias_ure@hotmail.com'
					}
				}
				else {
					r = {
						success: false,
						token: '',
						message: 'Datos de ingreso incorrectos.'
					}
				}*/

			}
			else {
				alert('No hay conexión al websocket');
			}
			
			//return r;
		}

		Ws.prototype.getComics = function( callbackFn ) {
			var that = this;
			if( that.connOpen ) {
				that.myWebSocket.send(JSON.stringify({type: 'comics', token: this.tokenValue}));
				that.myWebSocket.onmessage = function( e ) {
					var r = JSON.parse(e.data);
					callbackFn(r);

				}
			}
			else {
				alert( 'Se ha perdido la conexión: ' + that.connOpen );
			}
		}

		return Ws;
	}());

	App.Ws = new Ws; // Exporto ws para ser utilizada en otro lugar

})( App || (App = {}) );