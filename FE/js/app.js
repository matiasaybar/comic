var App;
(function( App ) {

	var comicStore = (function(){

		function comicStore() {
			this.conn;
		}

		comicStore.prototype.init = function() {
			//Crear conexión
			this.conn = App.Ws.create();
			// Verificar token
			var token = this.getToken('token');
			if( !token ) {
				// No hay sesión, pedir inicio de sesión.
				App.Views.showElement('login');
			}
			else {
				this.showUtils();
			}			
			
		}

		comicStore.prototype.login = function( user, pass ) {
			var that = this;
			var conn = App.Ws.login( user, pass, function( conn ) {

				if( conn.success ) {
					that.setToken( 'token', conn.token ); // Seteo el token
					that.setToken( 'user', user );
					that.setToken( 'email', conn.email );
					App.Views.hideElement('login'); // Oculto el login
					that.showUtils(); // muestra funciones y contenido
					App.Views.setMessage('info-email', conn.email);
					App.Views.setMessage('info-username', user);
				}
				else {
					App.Views.setMessage('aviso', conn.message);
				}

			} );
		}

		comicStore.prototype.logout = function() {
			this.clearToken('token');
			this.clearToken('user');
			this.clearToken('email');
			window.location.reload(true);
		}

		comicStore.prototype.getToken = function( key ) {
			// Get Session (if exists, in localStorage)
			return window.localStorage.getItem( key ) || false;
		}

		comicStore.prototype.setToken = function( key, value ) {
			// Execute in login
			window.localStorage.setItem( key, value );
		}

		comicStore.prototype.clearToken = function( key ) {
			window.localStorage.removeItem( key );
		}

		comicStore.prototype.showComics = function() {
			this.clearItemsList();
			App.Views.setMessage('aviso-items', 'Cargando...');
			App.Ws.getComics(function( listComics ) {
				for(var i in listComics) {
					App.Views.createElement('items', listComics[i]);
					App.Views.setMessage('aviso-items', '');
				}
			});
		}

		comicStore.prototype.showComicsFiltered = function( type, value ) {
			var countItems = 0;
			this.clearItemsList();
			App.Views.setMessage('aviso-items', 'Cargando...'); // Aviso de busqueda

			App.Ws.getComics(function( listComics ) {
				for(var i in listComics) {
					switch( type ) {
						case 'genre':
							if( listComics[i].genre == value ) {
								App.Views.createElement('items', listComics[i]);
								countItems++;
							}
						break;
						case 'edition':
							if( listComics[i].edition == value ) {
								App.Views.createElement('items', listComics[i]);
								countItems++;
							}
						break;
						case 'character':
							if( listComics[i].character == value ) {
								App.Views.createElement('items', listComics[i]);
								countItems++;
							}
						break;
						default:
							alert('Falló filtro.');
						break;
					}
				}

				if (countItems == 0) {
					App.Views.setMessage('aviso-items', 'No se encontraron resultados');
				}
				else {
					App.Views.setMessage('aviso-items', '');
				}
			});
		}

		comicStore.prototype.showUtils = function() {
			App.Views.showElement('articles'); // Muestro la página
			App.Views.showElement('login-options');
			this.showComics();
		}
		comicStore.prototype.hideUtils =function() {

		}

		comicStore.prototype.clearItemsList = function() {
			document.getElementById('items').innerHTML = '';
		}

		return comicStore;

	}());

	App.comicStore = new comicStore;

})( App || (App = {}) );

App.comicStore.init();

// Fede BitBucket: c130710a