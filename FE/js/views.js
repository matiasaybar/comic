var App;
(function( App ) {

	var Views = (function() {

		function Views() {

		}

		Views.prototype.showElement = function ( element ) {
			document.getElementById(element).style.display = 'block';
		}

		Views.prototype.hideElement = function ( element ) {
			document.getElementById(element).style.display = 'none';
		}

		Views.prototype.createElement = function ( parent, item ) {
			//console.log(item);
			//console.log(typeof item);
			if(typeof item == 'object') {
				window.document.getElementById(parent).innerHTML += '<div class="col-md-4"><h3>' + item.character + '</h3><h6>' + item.title + '</h6><p class="small">' + item.genre + ' [ ' + item.edition + ' ]</p><img src="' + item.img + '" class="img-responsive"><p>' + item.description + '</p></div>';
			}
		}

		Views.prototype.setMessage = function ( element, message ) {
			document.getElementById(element).innerHTML = message;
		}

		return Views;

	}());

	App.Views = new Views;

})( App || (App = {}) );